import React from 'react'
import { Router, Link } from 'react-static'
import { hot } from 'react-hot-loader'
import Routes from 'react-static-routes'
import Navigation from 'containers/Navigation'
import {Helmet} from "react-helmet";

import {
  Container, Row, Col
} from 'reactstrap';

import './app.css'





const App = () => (
  <Router>
    <div>
      <div className="wrapper">
        <Helmet>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
          <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" />
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />
        </Helmet>
        <Navigation />
        <div className="content">
          <Routes />
        </div>
        <footer>
          <Container>
            <Row><Col sm="12" className="text-center">&copy; 2020  The Calvert Companies, Inc.  |  12437 E. 60th Street  Tulsa, Oklahoma 74146  |  <a href="tel:800-722-8550">(800) 722-8550</a> or <a href="tel:918-461-0609">(918) 461-0609</a></Col></Row>
          </Container>
        </footer>
      </div>
    </div>
  </Router>
)

export default hot(module)(App)
