import React from 'react';
import {
  Container, Row, Col,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    let { scrollClass } = this.state
    return (
      <div>
        <Navbar color="faded" light fixed expand="md">
            <Container><Row><Col xs="12">
          <NavbarBrand href="/"><img src="/uploads/calvertco.svg" /></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="mailto:owner@calvertco.com"><i className="far fa-envelope"></i></NavLink>
              </NavItem>
            </Nav>
          </Collapse>
          </Col></Row></Container>
        </Navbar>
      </div>
    );
  }
}