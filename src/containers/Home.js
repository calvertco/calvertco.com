import React from 'react'
import { withSiteData } from 'react-static'
//
import logoImg from '../logo.png'

export default withSiteData(() => (
  <div>
    <section id="home">
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <h1>Pairing design with purpose<br /><span>to lift your company’s brand.</span></h1>
           </div>
        </div>
      </div>
    </section>
    <section id="pi">
      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <img className="brand-logo pi" src="/uploads/proimage.svg" />
          </div>
          <div className="col-sm-6">
            <h2>Custom Packaging</h2>
            <p><strong>Professional Image</strong> specializes in custom, high-end packaging for a multitude of markets including gourmet foods, health and personal care, pet care, home products, and other retail and wholesale industries. Services include graphic design through to fulfillment and warehousing.</p>
            <a className="btn btn-primary" href="https://www.professionalimagepackaging.com/?utm_source=calvertco">Visit Website</a>
            <div className="brand-links">
              <a href="https://www.facebook.com/proimage.tulsa"><i className="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/pipackaging"><i className="fab fa-twitter-square"></i></a>
              <a href="https://www.instagram.com/professionalimage"><i className="fab fa-instagram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="asb">
      <div className="container">
        <div className="row">
          <div className="col-sm-6 order-sm-2">
            <img className="brand-logo asb" src="/uploads/aspecialtybox.svg" />
          </div>
          <div className="col-sm-6">
            <h2>Stock Packaging</h2>
            <p><strong>aspecialtybox.com</strong> offers stock designer gift packaging at wholesale prices for confection, gourmet food, and gift industries. Benchmark lines include classic and contemporary heart-shaped boxes, tiered gift boxes, whimsical and elegant designer boxes, as well as all-occasion and seasonal boxes. Custom and private label packaging is available. International and USA manufacturing.</p>
            <a className="btn btn-primary" href="https://www.aspecialtybox.com/?utm_source=calvertco">Visit Website</a>
            <div className="brand-links">
              <a href="https://www.facebook.com/aspecialtybox"><i className="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/aspecialtybox"><i className="fab fa-twitter-square"></i></a>
              <a href="https://www.instagram.com/aspecialtybox"><i className="fab fa-instagram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="pi3d">
      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <img className="brand-logo pi3d" src="/uploads/proimage3D.svg" />
          </div>
          <div className="col-sm-6">
            <h2>3D Printing Technology</h2>
            <p><strong>ProImage 3D</strong> harnesses the power of 3D Scanning, 3D Design and 3D Printing to make your prototype a reality. 3D printed models enable you and all others on your design team to make educated decisions resulting in fewer changes, better product designs, and lower costs giving you a valuable edge in a very competitive marketplace.</p>
            <a className="btn btn-primary" href="https://www.proimage3d.com/?utm_source=calvertco">Visit Website</a>
            <div className="brand-links">
              <a href="https://www.facebook.com/pro3drp"><i className="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/pro3drp"><i className="fab fa-twitter-square"></i></a>
              <a href="https://www.instagram.com/proimage3d"><i className="fab fa-instagram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
))
