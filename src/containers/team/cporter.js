import React from 'react'
import {Helmet} from "react-helmet";
//

export default () => (
  <div>
        <Helmet>
            <body class='single-team' />
        </Helmet>
        <section id="logo">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        <img className="brand-logo pi" src="/uploads/proimage.svg" />
                    </div>
                </div>
            </div>
        </section>
        <section id="single-profile">
            <div className="container">
                <div className="col-xs-12">
                    <a className="btn btn-primary" href="/uploads/vcards/cporter.vcf">Import Contact To Phone</a>
                </div>
                <div className="col-xs-12 col-sm-6">
                    <div className="profile-pic">
                        <img src="/uploads/headshots/cporter-600x600.jpg" />
                    </div>
                </div>
                <div className="col-xs-12 col-sm-6">
                    <h1>Christopher Porter</h1>
                    <h2>Director of Sales &amp; Marketing</h2>
                    <div className="contact-links text-center">
                        <div className="col-xs-12">
                            <a href="mailto:cporter@calvertco.com">cporter@calvertco.com</a>
                        </div>
                        <div className="col-xs-12">
                            Office: <a className="phone-link" href="tel:918-461-0609">918.461.0609</a> x 128
                        </div>
                        <div className="col-xs-12">
                            Cell: <a className="phone-link" href="tel:">918.461.0609</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  </div>
)