import axios from 'axios'


export default {

  siteRoot: 'https://calvertco.com',

  getSiteData: () => ({
    title: 'The Calvet Companies',
  }),
  getRoutes: async () => {

    return [
      {
        path: '/',
        component: 'src/containers/Home',
      },
      {
        path: '/team',
        component: 'src/containers/Home',
      },
      {
        path: '/team/cporter',
        component: 'src/containers/team/cporter',
      },
      {
        is404: true,
        component: 'src/containers/404',
      },
    ]
  },
}
